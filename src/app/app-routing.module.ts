import { Routes } from '@angular/router';
import { DashboardComponent } from '../app/dashboard/dashboard.component';
import { EditDepartmentComponent } from '../app/edit-department/edit-department.component';
import { ShowDepartmentComponent } from '../app/show-department/show-department.component';
export const routes: Routes = [

    {
        path: '', redirectTo: '/', pathMatch: 'full'
    },
    {
        path: '',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: ShowDepartmentComponent
            },
            {
                path: 'Dashboard/showDepartment',
                component: ShowDepartmentComponent
            }
        ]
    },
    {
        path: '**',
        redirectTo: ''
    }
]