import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DepartmentsData {
  id: number;
  deptName: string;
  deptHOD: string;
}

@Component({
  selector: 'app-edit-department',
  templateUrl: './edit-department.component.html',
  styleUrls: ['./edit-department.component.css']
})

export class EditDepartmentComponent implements OnInit {

  action:string;
  local_data:any;
  readonly: boolean;

  constructor(
    public dialogRef: MatDialogRef<EditDepartmentComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: DepartmentsData) {
    this.local_data = {...data};
    this.action = this.local_data.action;
    this.readonly =  this.action === "Update" ?  true : false;
  }

  ngOnInit() {
  } 

  doAction(){
    this.dialogRef.close({event:this.action,data:this.local_data});
  }

  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }

}

