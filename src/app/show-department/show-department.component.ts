import { Component, ViewChild, OnInit } from '@angular/core';

import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

import { EditDepartmentComponent } from '../edit-department/edit-department.component';
import { DepartmentService } from '../services/department.service';

export interface DepartmentsData {
  id: number;
  deptName: string;
  deptHOD: string;
}

@Component({
  selector: 'app-show-department',
  templateUrl: './show-department.component.html',
  styleUrls: ['./show-department.component.css']
})

export class ShowDepartmentComponent implements OnInit {
  displayedColumns: string[] = ['id', 'deptName', 'deptHOD', 'action'];
  dataSource: DepartmentsData[] = [];

  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(public dialog: MatDialog, private _service: DepartmentService) { }

  ngOnInit() {
    this.getDepartmentList();
  }

  /**
   * method to get the department list on load
   */
  getDepartmentList() {
    this._service.getDepartmentDetails().subscribe(data => {
      this.dataSource = data;
    })
  }

  /**
   * method to open the dialog box
   * @param action 
   * @param obj 
   */
  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(EditDepartmentComponent, {
      width: 'auto',
      height: 'auto',
      disableClose: true,
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Add') {
        this.addRowData(result.data);
      }
      else if (result.event == 'Update') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }

  /**
   * method to add the new department
   * @param row_obj 
   */
  addRowData(row_obj) {
    const data = {
      id: row_obj.id,
      deptName: row_obj.deptName,
      deptHOD: row_obj.deptHOD
    };
    this._service.addNewDepartment(data).subscribe(res => {
      if (res) {
        alert('Added Successfully !');
        this.dataSource.push({
          id: row_obj.id,
          deptName: row_obj.deptName,
          deptHOD: row_obj.deptHOD
        });
        this.table.renderRows();
      }
      else {
        alert('Something Went Wrong. Try Again !');
      }
    });
  }

  /**
   * method to update the department details in memory
   * @param row_obj 
   */
  updateRowData(row_obj) {
    this.dataSource = this.dataSource.filter((value, key) => {
      if (value.id == row_obj.id) {
        alert('Updated Successfully !');
        value.deptName = row_obj.deptName;
        value.deptHOD = row_obj.deptHOD;
      }
      return true;
    });
  }

  /**
   * method to delete the department details in memory
   * @param row_obj 
   */
  deleteRowData(row_obj) {
    alert('Deleted Successfully !');
    this.dataSource = this.dataSource.filter((value, key) => {
      return value.id != row_obj.id;
    });
  }

  /**
   * method to update the department details with api call
   * @param row_obj 
   */
  updateRowDetail(row_obj) {
    const data = {
      id: row_obj.id,
      deptName: row_obj.deptName,
      deptHOD: row_obj.deptHOD
    };
    this._service.updateDepartment(row_obj.id, data).subscribe(res => {
      if (res) {
        alert('Updated Successfully !');
        this.dataSource = this.dataSource.filter((value, key) => {
          if (value.id == row_obj.id) {
            value.deptName = row_obj.deptName;
            value.deptHOD = row_obj.deptHOD;
          }
          return true;
        });
      }
      else {
        alert('Something Went Wrong. Try Again !');
      }
    });

  }

  /**
   * method to delete the department details with api call
   * @param row_obj 
   */
  deleteRowDetail(row_obj) {
    this._service.deleteDepartment(row_obj.id).subscribe(res => {
      if (res) {
        alert('Deleted Successfully !');
        this.dataSource = this.dataSource.filter((value, key) => {
          return value.id != row_obj.id;
        });
      }
      else {
        alert('Something Went Wrong. Try Again !');
      }
    })
  }

}
