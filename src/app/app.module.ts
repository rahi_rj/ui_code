import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditDepartmentComponent } from './edit-department/edit-department.component';
import { ShowDepartmentComponent } from './show-department/show-department.component';
import { MaterialModule } from './material.module';
import { routes } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    EditDepartmentComponent,
    ShowDepartmentComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    HttpClientModule
  ],

  entryComponents: [
   EditDepartmentComponent
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
