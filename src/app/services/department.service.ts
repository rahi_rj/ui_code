import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { BehaviorSubject, Observable, observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class DepartmentService {

  constructor(private http:HttpClient) { }

  public getDepartmentDetails() : Observable<any>{
    const url = 'https://localhost:44323/api/department/getDepartments';
    return this.http.get(url);
  }

  public addNewDepartment(data): Observable<any>{
    const url = 'https://localhost:44323/api/department/addDepartment';
    return this.http.post(url, data);
  }

  public updateDepartment(id, data): Observable<any>{
    const url = `https://localhost:44323/api/department/updateDepartment?id=${id}`;
    return this.http.put(url, data);
  }

  public deleteDepartment(id): Observable<any>{
    const url = `https://localhost:44323/api/department/deleteDepartment?id=${id}`;
    return this.http.delete(url);
  }
}
